package angajatiapp;

import angajatiapp.controller.DidacticFunction;
import angajatiapp.controller.EmployeeController;
import angajatiapp.model.Employee;
import angajatiapp.repository.EmployeeImpl;
import angajatiapp.repository.EmployeeMock;
import angajatiapp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ModifyEmployeeTest {

    @Test
    public void testCfg1() {
        EmployeeMock employeesMock = new EmployeeMock();
        Employee employee = null;
        employeesMock.modifyEmployeeFunction ( employee, DidacticFunction.LECTURER );
        employee = employeesMock.getEmployeeList().get(0);
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
    }

    @Test
    public void testCfg2() {
        EmployeeMock employeesMock = new EmployeeMock();
        Employee employee = employeesMock.getEmployeeList().get(0);
        employeesMock.modifyEmployeeFunction ( employee, DidacticFunction.LECTURER );
        employee = employeesMock.getEmployeeList().get(0);
        assertEquals(DidacticFunction.LECTURER, employee.getFunction());
    }

    @Test
    public void testCfg3() {
        EmployeeMock employeesMock = new EmployeeMock();
        Employee employee = new Employee("Ana", "Baesu", "1234567890899", DidacticFunction.ASISTENT, 2500d);
        employee.setId(100);
        employeesMock.modifyEmployeeFunction ( employee, DidacticFunction.LECTURER );
        employee = employeesMock.getEmployeeList().get(0);
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
    }
}
