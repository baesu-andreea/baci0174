package angajatiapp;

import angajatiapp.controller.DidacticFunction;
import angajatiapp.controller.EmployeeController;
import angajatiapp.model.Employee;
import angajatiapp.repository.EmployeeImpl;
import angajatiapp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;

import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeTest {
    Employee e1, e2, e3;

    @BeforeEach
    public void SetUp() {
        e1 = new Employee();
        e2 = null;
        e3 = new Employee();
        e3.setFirstName("Ioana");
        e3.setLastName("Popescu");
        e3.setCnp("1234567890123");
        e3.setId(3);
        e3.setFunction(DidacticFunction.ASISTENT);
        e3.setSalary(1000.5);
        System.out.println("Before Test");

    }

    @Test
    void getName_1() {
        try {
            e2.getFirstName();
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("e2 does not have any name");
    }

    @Test
    void getName_2() {
        assertEquals("", e1.getFirstName());
        System.out.println("e1 has empty first name");
    }

    @Disabled
    @Test
    void getName_Disabled() {
        assertEquals(" ", e1.getFirstName());
        System.out.println("e1 has empty first name");
    }

    @Test
    void getName_compus() {
        assertEquals("Ioana", e3.getFirstName());
        assertEquals("", e1.getFirstName());
        try {
            assertEquals("", e2.getFirstName());
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("3 getName tests ok");
    }

    @Test
    void getName_test_all() {
        assertAll("Employee", (Executable)
                        (Executable) () -> assertEquals("", e1.getFirstName()),
                (Executable) () -> assertEquals(null, e2),
                (Executable) () -> assertEquals("Ioana", e3.getFirstName()));
        System.out.println("Test all ok");
    }

    @Test
    void setName() {
        assertEquals("", e1.getFirstName());
        e1.setFirstName("Maria");
        assertNotEquals("", e1.getFirstName());
        assertEquals("Maria", e1.getFirstName());
        System.out.println("setName test ok");
    }

    @Test
    void constructorName() {
        Employee aux = new Employee();
        assertEquals(e1.getFirstName(), aux.getFirstName());
        assertNotEquals(null, aux);
        System.out.println("constructor test ok");
    }
    @Test
    @Order(3)
    void getLastName_1() {
        try {
            e2.getLastName();
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("e2 does not have name");
    }
    @Test
    @Order(2)
    void getLastName_compus() {
        assertEquals("Popescu", e3.getLastName());
        assertEquals("", e1.getLastName());
        try {
            assertEquals("", e2.getLastName());
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("3 getName tests ok");
    }
//Exceptii la executia testelor
    @Test
    void getCnp() {
        assertEquals("1234567890", e3.getCnp());
        assertEquals("", e1.getCnp());
        try {
            assertEquals("", e2.getCnp());
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("3 cnpTest ok");
    }

    @AfterEach
    public void TearDown() {
        e1 = null;
        e2 = null;
        e3 = null;
        System.out.println("After Test");
    }
    //timeout: varianta cu adnotarea Timeout
    @Test
    //@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    //@Timeout(5)
    public void findEmployeeById() {
        try {
            //Thread.sleep(100);
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        employeeController.addEmployee(e1);
        employeeController.addEmployee(e3);
        Employee employee = employeeController.findEmployeeById(3);
        assertEquals(employee.getFirstName(), "Ioana");
        System.out.println("search with timeout ok");
    }

    @ParameterizedTest
    @ValueSource(strings = {"Andrei"})
    void testParametrizatSetandGetFirstName(String name) {
        e1.setFirstName(name);
        assertEquals(name, e1.getFirstName());
        System.out.println("Parametrized Set&Get with "+name+" ok");
    }
}

