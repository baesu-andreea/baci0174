package angajatiapp;

import angajatiapp.controller.DidacticFunction;
import angajatiapp.controller.EmployeeController;
import angajatiapp.model.Employee;
import angajatiapp.repository.EmployeeImpl;
import angajatiapp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddEmployeeTest {

    //in EmployeeController addEmployee era public void si pentru teste aveam nevoie de public boolean

    @Test
    public void testEcp1() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Maria";
        String lastName = "Ionescu";
        String employeeCnp = "1234567890123";
        DidacticFunction employeeFunction = DidacticFunction.ASISTENT;
        Double employeeSalary = 1000.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    public void testEcp2() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Vasilescu";
        String lastName = "Radu";
        String employeeCnp = "12222222";
        DidacticFunction employeeFunction = DidacticFunction.ASISTENT;
        Double employeeSalary = 1000.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    public void testEcp3() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Popescu";
        String lastName = "Vasile";
        String employeeCnp = "1231231231231";
        DidacticFunction employeeFunction = DidacticFunction.LECTURER;
        Double employeeSalary = 1000.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    public void testEcp4() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Marinescu";
        String lastName = "Ana";
        String employeeCnp = "1234123412341";
        DidacticFunction employeeFunction = DidacticFunction.TEACHER;
        Double employeeSalary = -500.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(false, result);
    }
    @Test
    public void testBva1() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Ionescu";
        String lastName = "Maria";
        String employeeCnp = "1234567890123";
        DidacticFunction employeeFunction = DidacticFunction.ASISTENT;
        Double employeeSalary = 1000.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(true, result);
    }
    @Test
    public void testBva2() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Vasilescu";
        String lastName = "Radu";
        String employeeCnp = "123456789012";
        DidacticFunction employeeFunction = DidacticFunction.ASISTENT;
        Double employeeSalary = 1000.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(false, result);
    }
    @Test
    public void testBva3() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Popescu";
        String lastName = "Vasile";
        String employeeCnp = "12345678901234";
        DidacticFunction employeeFunction = DidacticFunction.LECTURER;
        Double employeeSalary = 1000.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(false, result);
    }
    @Test
    public void testBva7() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Miron";
        String lastName = "Viorica";
        String employeeCnp = "1234567123456";
        DidacticFunction employeeFunction = DidacticFunction.TEACHER;
        Double employeeSalary = 0.01;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(true, result);
    }
    @Test
    public void testBva8() {
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        String firstName = "Marinescu";
        String lastName = "Ana";
        String employeeCnp = "1234567890123";
        DidacticFunction employeeFunction = DidacticFunction.ASISTENT;
        Double employeeSalary = 0.0;
        Employee employee = new Employee(firstName, lastName, employeeCnp, employeeFunction,employeeSalary);

        boolean result = employeeController.addEmployee(employee);
        assertEquals(false, result);
    }
}